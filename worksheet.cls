\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{worksheet}

\LoadClass{article}

%\DeclareOption{examplelecture}{\newcommand{\thelecture}{zur Einführung in die Vorlesungen II}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%\DeclareOption*{\PackageWarning{worksheet}{Unknown ‘\CurrentOption’}}
\ProcessOptions\relax

%\RequirePackage{kvoptions}

\RequirePackage{xparse}

%

\RequirePackage[utf8]{inputenc} % UTF-8

\RequirePackage{geometry} % page layout

\RequirePackage[ngerman]{babel} % language specifics

%

%\RequirePackage{url} % ?
%\RequirePackage{hyperref} % ?

\RequirePackage[multiple]{footmisc} % better footnotes

\RequirePackage{fancyhdr} % add header and footer

\RequirePackage{titlesec} % add ability to change title format

%

\RequirePackage{graphicx} % graphics

\RequirePackage{floatflt} % floating wrapper

\RequirePackage{tabularx} % cooler tables
\RequirePackage{multirow} % multiple rows in tabularx

%\RequirePackage{ulem} % more emphesising options

\RequirePackage{enumerate}

\RequirePackage{xcolor}

%

\RequirePackage[version=4]{mhchem} % chemistry
\RequirePackage{chemfig} % chemistry

%\RequirePackage{gensymb} % more stable unit symbols and notation
%\RequirePackage{textcomp} % more stable unit symbols and notation
\RequirePackage{siunitx} % si unit notation
\RequirePackage[right]{eurosym}

%

\RequirePackage{amssymb} % math symbols
\RequirePackage{amsthm} % math theorems and proofs
\RequirePackage{amsmath}
\RequirePackage{mathtools} % more math symbols and tools

\RequirePackage{cancel}

%\RequirePackage{starfont}
\RequirePackage{wasysym}
%\RequirePackage{mathabx} % astronomical symbols

%

\RequirePackage{makeidx} % index

\RequirePackage[backend=biber,style=numeric]{biblatex} % bibliography

%

\RequirePackage{tikz}
\RequirePackage[european, siunitx]{circuitikz}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\thename}{}
\newcommand{\themtknr}{}
\newcommand{\theblatt}{}

\@ifundefined{foo}{\newcommand{\thelecture}{}}{}

\newcommand{\name}[1]{\renewcommand{\thename}{#1}}
\newcommand{\mtknr}[1]{\renewcommand{\themtknr}{#1}}
\newcommand{\blatt}[1]{\renewcommand{\theblatt}{#1}}
\newcommand{\lecture}[1]{\renewcommand{\thelecture}{#1}}

%

\addbibresource{bibliography}

\makeindex

%

\geometry{inner=2cm,outer=5cm,top=2cm,bottom=2cm,twoside,a4paper}
%\geometry{inner=1cm,outer=1cm,top=1.5cm,bottom=1.5cm,twoside,a4paper}%,landscape}

\renewcommand{\thesubsubsection}{\thesubsection.\alph{subsubsection}}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{.2mm}
\renewcommand{\footrulewidth}{.2mm}
\fancyfoot[C]{\thepage}
\fancyhead[OL,ER]{Mtknr.: \themtknr}
\fancyhead[EL,OR]{Name: \thename}

\author{\thename}

\title{Übungsaufgaben \theblatt\ \thelecture}

\renewcommand{\thesubsubsection}{\thesubsection.\alph{subsubsection}}

\titleformat*{\section}{\LARGE\bfseries}
\titleformat{\subsection}{\large\bfseries}{}{0em}{}

\setlength{\parindent}{0mm}
\setlength{\parskip}{2mm}

%

\DeclareMathOperator{\arcosh}{arcosh}
\DeclareMathOperator{\Mat}{Mat}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\dvgnc}{div}
\DeclareMathOperator{\rot}{rot}
\DeclareMathOperator{\grad}{grad}
\DeclareMathOperator{\Id}{Id}
\DeclareMathOperator{\Hess}{Hess}
\DeclareMathOperator{\rg}{rg}

\newcommand{\de}{\mathrm d}
\newcommand{\del}{\partial}

\newcommand{\laplace}{\nabla^2}
%\newcommand{\laplace}{\Delta}

%\renewcommand{\qed}{\hfill$\square$}
\renewcommand{\qed}{\hfill q.\,e.\,d.}
\newcommand{\obda}{o.\,B.\,d.\,A.}
\let\conj\overline

\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\K}{\ensuremath{\mathbb{K}}}

\newcommand{\wo}{\ensuremath{\setminus}}

\theoremstyle{definition}
\newtheorem{proposition}{Behauptung}[subsubsection]
\newtheorem{definition}{Definition}[subsubsection]
\newtheorem{theorem}{Satz}[subsubsection]
\renewenvironment{proof}{\noindent\textit{Beweis:} }{\qed}

\sisetup{per-mode = fraction, exponent-product = \cdot, output-decimal-marker = \text{,}}
%\sisetup{inter-unit-product = \ensuremath{\cdot}}

\newcommand{\VEC}[1]{\begin{pmatrix}#1\end{pmatrix}}

%

\newcommand{\todo}{{\color{red} \textbf{YET TO BE FINISHED}}}

\newcommand{\done}{\renewcommand{\todo}{\ClassError{worksheet}{Unfinished exercise found!}{Remove \textbackslash todo when you are done. If you are not done, remove \textbackslash done...}}}

%

\usetikzlibrary{decorations.pathmorphing,patterns}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{quotes,angles}
\usetikzlibrary{babel}